#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#ifndef ST_LIST_H
#define ST_LIST_H

#define STATIONLIST_FILENAME	"stations.txt"

typedef struct
{
	char station_name[255];
	char station_url[1024];
} stationlist_item;

static stationlist_item stationlist[1024] = {0};
static int stations_count = 0;

void stationlist_read();
int get_stations_count();
void stationlist_add_station(char *station_name, char *station_url);
stationlist_item stationlist_get_station(int num);

#endif
