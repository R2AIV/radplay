all:
	gcc -o radplay stationlist.c radplay.c -lmpg123 -lao -lcrypto -lssl -lncurses
	gcc -o add_st stationlist.c add_st.c
	gcc -o showlist stationlist.c showlist.c
	gcc -o sslc sslc.c -lssl

clean:
	rm radplay add_st showlist sslc
