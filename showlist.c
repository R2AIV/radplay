#include "stationlist.h"

int main(void)
{
	int station_count = 0;

	stationlist_read();

	station_count = get_stations_count();

	for(int i=0;i<station_count-1; i++)
	{
		printf("%d\t\x1b[35m%s\x1b[0m\t\t\x1b[32m%s\x1b[0m\r\n",\
				i,\
				stationlist_get_station(i).station_name,
				stationlist_get_station(i).station_url);
	}

}
