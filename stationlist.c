#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "stationlist.h"


void stationlist_read()
{
	FILE *st_list_fd = fopen(STATIONLIST_FILENAME, "r");

	char tmp_name[100] = {0};
	char tmp_url[100] = {0};

	if(st_list_fd == NULL)
	{
		printf("Error opening stations list file!\r\n");
		exit(-1);
	}
	unsigned int station_num = 0;

	while(!feof(st_list_fd))
	{
		fscanf(st_list_fd, "%s", tmp_name);
		fscanf(st_list_fd, "%s", tmp_url);

		strcpy(stationlist[station_num].station_name, tmp_name);
		strcpy(stationlist[station_num].station_url, tmp_url);

		station_num++;
		stations_count = station_num;
	}
	fclose(st_list_fd);
}

int get_stations_count()
{
	return stations_count;
}


void stationlist_add_station(char *station_name, char *station_url)
{
	FILE *st_list_fd = fopen(STATIONLIST_FILENAME, "a+");
	fprintf(st_list_fd, "%s %s", station_name, station_url);
	fclose(st_list_fd);
}

stationlist_item stationlist_get_station(int num)
{
	return stationlist[num];
}
