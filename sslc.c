#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <openssl/ssl.h>

#define REQUEST		"GET /live HTTP/1.1\r\n\r\n"

int connect_tcp()
{
	int stat = 0;
	struct sockaddr_in srv;
	int sock = socket(AF_INET, SOCK_STREAM, 0);

	srv.sin_addr.s_addr = inet_addr("185.41.187.142");
	srv.sin_port = htons(443);
	srv.sin_family = AF_INET;

	stat = connect(sock, (struct sockaddr *)&srv, sizeof(srv));

	if(stat != 0)
	{
		printf("Connect error!\r\n");
		exit(-1);		
	}

	printf("Connected OK!\r\n");

	return sock;
};
	
int tcp_srv()
{
	int srv_sock = socket(AF_INET, SOCK_STREAM, 0);
	int stat = 0;
	struct sockaddr_in srv_addr;

	srv_addr.sin_addr.s_addr = INADDR_ANY;
	srv_addr.sin_port = htons(6767);
	srv_addr.sin_family = AF_INET;

	stat = bind(srv_sock, (struct sockaddr *)&srv_addr, sizeof(srv_addr));

	if(stat != 0)
	{
		printf("Server bind error!\r\n");
	}

	listen(srv_sock, 2048);

	return srv_sock;
}

int main(void)
{
	// File output
	int out_file;

	// TCP socket	
	int sock = 0;
	int srv_sock = 0;
	int srv_acc_sock = 0;

	// Data buffer
	char data_buff[2048] = {0};
	int data_bytes = 0;

	printf("HTTPS to HTTP translator for remote streaming.\r\n");
	printf("Please connect to \x1b[34mhttp://localhost:6767\x1b[0m\r\n");

	// SSL Setup
	SSL *ssl;
	SSL_CTX *ctx = SSL_CTX_new(TLS_method());
	ssl = SSL_new(ctx);

	sock = connect_tcp();
	srv_sock = tcp_srv();

	SSL_set_fd(ssl, sock);
	SSL_connect(ssl);

	SSL_write(ssl, REQUEST, strlen(REQUEST));

	while(1)
	{
		srv_acc_sock = accept(srv_sock, NULL, NULL);
		printf("Client connected!\r\n");

		data_bytes = 1;

		while(1)
		{
			SSL_read_ex(ssl, data_buff, sizeof(data_buff), (size_t *)&data_bytes);
			write(srv_acc_sock, data_buff, data_bytes);		
		}
	};
}
