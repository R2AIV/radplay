#include "stationlist.h"

int main(void)
{
	char name[100] = {0};
	char url[255] = {0};

	printf("Enter station name(without spaces): ");
	scanf("%s", name);
	printf("Enter station url: ");
	scanf("%s", url);

	stationlist_add_station(name, url);

	return 0;

}
