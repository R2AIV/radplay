// Standard
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

// Network
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>

// Crypto
#include <openssl/ssl.h>

//Audio
#include <mpg123.h>
#include <ao/ao.h>

// NCurses
#include <curses.h>

#include "stationlist.h"

#define BITS 8

typedef struct
{
	char protocol[10];	// HTTP of HTTPS
	char hostname[100];	// hostname
	uint16_t port;		// port
	char path[100];		// path arter port in url
} stream_info;

// Main Window
WINDOW *main_win;


// Parsing stream URL
stream_info parse_url(char *url)
{
	stream_info parsed_info;
	memset((void *)&parsed_info, 0, sizeof(parsed_info));

	char source_url[100] = {0};

	char tmp_protocol[10] = {0};
	char tmp_hostname[100] = {0};
	char tmp_path[100] = {0};

	int url_pos = 0;

	strcpy(source_url, url);

	// Parsing url string
	char *pch = strtok(source_url, ":/");
	while(pch != NULL)
	{
		switch(url_pos)
		{
			case 0: strcpy(parsed_info.protocol, pch); break;
			case 1: strcpy(parsed_info.hostname, pch); break;
			case 2: parsed_info.port = atoi(pch); break;
			case 3: strcpy(parsed_info.path, pch); break;
		}

		pch = strtok(NULL, ":/");
		url_pos++;
	}


	// Incorrect URL processing
	if(url_pos != 4)
	{
		printf("Incorrect URL!\r\n");
		printf("URL must be look like:\r\n");
		printf("\x1b[32m[http://hostname:port/path]\x1b[0m\r\n\r\n");
		exit(0);
	}

	if(strcmp(parsed_info.protocol, "https") == 0)
	{
		printf("Currently HTTPS is not supported!\r\n");
		exit(0);
	}

	if(strcmp(parsed_info.protocol, "http") != 0)
	{
		printf("Unknown transport protocol in URL!\r\n");
		exit(0);
	}
	
	return parsed_info;
}

// Create and connect to radio server TCP/IP socker
int tcp_connect(stream_info stream_url)
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	int stat = 0;
	uint8_t byte = 0;
	
	struct sockaddr_in srv_addr;
	struct hostent *hn;

	char srv_hostname[100] = {0};
	char srv_ip_addr[20] = {0};
	
	char http_request[100] = {0};

	strcpy(srv_hostname, stream_url.hostname);

	hn = gethostbyname(srv_hostname);
	strcpy(srv_ip_addr, inet_ntoa(*((struct in_addr *)hn->h_addr_list[0])));

	printf("Server host name: \x1b[33m%s\x1b[0m\r\n", srv_hostname);
	printf("Server IP address: \x1b[33m%s\x1b[0m\r\n", srv_ip_addr);
	printf("Server port: \x1b[33m%d\x1b[0m\r\n", stream_url.port);

	srv_addr.sin_addr.s_addr = inet_addr(srv_ip_addr);
	srv_addr.sin_port = htons(stream_url.port);
	srv_addr.sin_family = AF_INET;

	stat = connect(sock, (struct sockaddr *)&srv_addr, sizeof(srv_addr));
	if(stat != 0)
	{
		printf("Can't connect to server!\r\n");
		return -1;
	}

	sleep(1);


	// Formatting HTTP request
	sprintf(http_request, "GET /");
	strcat(http_request, stream_url.path);
	strcat(http_request, " HTTP/1.1\r\n\r\n");

	write(sock, http_request, strlen(http_request));
	return sock;
}

// Start to playing stream
void play(int conn_sock)
{
    mpg123_handle *mh;
    unsigned char *buffer;
    size_t buffer_size;
    size_t done;
    int err;

    int driver;
	ao_device *dev;

    ao_sample_format format;
    int channels, encoding;
    long rate;

	int sock = conn_sock;

    	// Init library functions
	ao_initialize();
    driver = ao_default_driver_id();
    mpg123_init();
    mh = mpg123_new(NULL, &err);
    buffer_size = mpg123_outblock(mh);
    buffer = (unsigned char*) malloc(buffer_size * sizeof(unsigned char));

	// Open socket by mpg123
	
	mpg123_open_fd(mh, sock);
    mpg123_getformat(mh, &rate, &channels, &encoding);

   	// Set output format for libao and start libao
	format.bits = mpg123_encsize(encoding) * BITS;
    	format.rate = rate;
    	format.channels = channels;
    	format.byte_format = AO_FMT_NATIVE;
    	format.matrix = 0;

	printf("Stream format: \r\n");
	printf("Rate: \x1b[32m%d\x1b[0m\r\n", rate);
	printf("Channels: \x1b[32m%d\x1b[0m\r\n", channels);

	// pid_t pid = fork();
	// if(pid != 0) exit(0);
	
    	dev = ao_open_live(driver, &format, NULL);

    	// Let's play!
	while (mpg123_read(mh, buffer, buffer_size, &done) == MPG123_OK)
        		ao_play(dev, (char*)buffer, done);
    	
	// Clean up and shutdown (currently not used)
	free(buffer);
	ao_close(dev);
    	mpg123_close(mh);
    	mpg123_delete(mh);
    	mpg123_exit();
    	ao_shutdown();
};



int main(int argc, char **argv)
{
	int chosen_station_num = 0;

	if(argc > 1)
	{
		chosen_station_num = atoi(argv[1]);
	}

	stationlist_read();

	stationlist_item current_station = stationlist_get_station(chosen_station_num);	

	stream_info stream_url;
	stream_url = parse_url(current_station.station_url);

	printf("RADPLAY 0.5 - free and open Internet Radio Player!\r\n");
	printf("by \x1b[32mR2AIV\x1b[0m FUCK ALL LICENSES!\r\n");	

	printf("Playing now: \x1b[35m%s\x1b[0m \x1b[32m%s\x1b[0m\r\n\r\n",\
			current_station.station_name, \
			current_station.station_url);


	stream_url = parse_url(current_station.station_url);

	int stream_sock = tcp_connect(stream_url);
	play(stream_sock);

	endwin();

}

